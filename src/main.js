import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import moment from 'moment'
import * as googlemaps from 'vue2-google-maps'

import "bootstrap/dist/css/bootstrap.min.css"
import './assets/css/main.less';

Vue.use(googlemaps, {
  load: {
    key: 'AIzaSyDuZ_rH5C9xXHrz5K-fnMSXoeHm1jAAONo'
  }
});

export const eventbus = new Vue({

  methods: {
    getCityWeatherData() {
      //let cities = [];
      let appid = 'ddd9cf1619b13fc2dcf5e7bd5c6f235c';
      let defaultcity = ''
      let promises = [];
      let citydata = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=1000-largest-us-cities-by-population-with-geographic-coordinates&sort=-rank&facet=city&facet=state'
      let tmp = axios.get(citydata).then((result) => {
        let tmpcities = result['data']['records']
        //remove 'wtf' var. no long needed nor being used
        let wtf = tmpcities.map((src) => {
          this.defaultcity = src['fields']['city']
          promises.push(axios.get('http://api.openweathermap.org/data/2.5/weather?q=' + this.defaultcity + '&appid=' + appid));
        });
        Promise.all(promises).then((result => {
          result.map((ele => {
            let city = {}
            city['temp'] = { ...ele['data']['main'] }
            city['name'] = ele['data']['name'];
            city['coords'] = {lat: ele['data']['coord']['lat'], lng: ele['data']['coord']['lon']};
            city['weather'] = { ...ele['data']['weather'] };
            this.cities.push(city);
          }))
          this.$emit('getWeatherCityData', this.cities);
        }));
      })
    }
  },
  created() {
    weatherAndCityData: {
      this.getCityWeatherData();
    }
  },
  data() {
    return {
      cities: []
    }
  }
})

new Vue({
  el: '#app',
  render: h => h(App)
})
